#ifndef TOO_MUCH_CODE_H
#define TOO_MUCH_CODE_H

#include <QWidget>
#include <QString>

namespace Ui {
    class TooMuchCode;
}

class TooMuchCode : public QWidget
{
        Q_OBJECT

    public:
        explicit TooMuchCode(QWidget *parent = 0);
        ~TooMuchCode();

    private slots:
       void compute_sum() const ;

    /**
        void record_change_in_1st_pair(int);
        void record_change_in_2nd_pair(int);

    private:
        void update_label() const ;
    */

    private:
        Ui::TooMuchCode *ui;
    /**
        int value_in_1st_pair;
        int value_in_2nd_pair;
    */
};

#endif // TOO_MUCH_CODE_H
