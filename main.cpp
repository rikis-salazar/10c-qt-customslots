#include "too_much_code.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    TooMuchCode w;
    w.show();

    return a.exec();
}
