#-------------------------------------------------
#
# Project created by QtCreator 2017-03-08T08:38:15
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = quick_and_dirty
TEMPLATE = app


SOURCES += main.cpp\
        too_much_code.cpp

HEADERS  += too_much_code.h

FORMS    += too_much_code.ui
