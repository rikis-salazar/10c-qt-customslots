#include "too_much_code.h"
#include "ui_too_much_code.h"

TooMuchCode::TooMuchCode(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TooMuchCode) //,
    // value_in_1st_pair(0),
    // value_in_2nd_pair(0)
{
    ui->setupUi(this);

    QObject::connect( ui->spinBox ,SIGNAL( valueChanged(int) ),
                      this, SLOT( compute_sum() )  );
    QObject::connect( ui->spinBox_2 ,SIGNAL( valueChanged(int) ),
                      this, SLOT( commpute_sum() ) );

    /**
    //  Note(s):
    //      1) with an incomplete set of connections, the
    //         result is not quite what one might expect, and
    //
    //      2) the parameter list in SIGNAL and SLOT do not
    //         match. This is OK because [the list] is not
    //         being used to transfer information.
    QObject::connect( ui->horizontalSlider ,SIGNAL( valueChanged(int) ),
                      this, SLOT( compute_sum() ) );
    QObject::connect( ui->horizontalSlider_2 ,SIGNAL( valueChanged(int) ),
                      this, SLOT( compute_sum() ) );
    */

}

TooMuchCode::~TooMuchCode()
{
    delete ui;
}

/**
void TooMuchCode::record_change_in_1st_pair(int value){
    // ...
}

void TooMuchCode::record_change_in_2nd_pair(int value){
    // ...
}

void TooMuchCode::update_label() const {
    // ...
}
*/

void TooMuchCode::compute_sum() const {

    // Wouldn't it be nice if we could get the needed values
    // directly from the spinBoxes???
    double x1 = ui->spinBox-> ... ;
    double x2 = ui->spinBox_2-> ... ;

    QString text( QString::number(x1) + " + " +
                  QString::number(x2) + " = " +
                  QString::number( x1 + x2 )
                );
    ui->label->setText(text);
}

